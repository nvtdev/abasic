import { StackNavigator } from 'react-navigation';
import { ProfileScreen } from './screens';

const ProfileTab = StackNavigator({
  Profile: {
    screen: ProfileScreen,
    path: '/',
    navigationOptions: () => ({
      title: 'Profile',
    }),
  },
});

export default ProfileTab;
