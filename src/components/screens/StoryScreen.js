import React from 'react';
import { Card, CardSection, Input, Button } from '../common';

const StoryScreen = () => (
  <Card>
    <CardSection>
      <Input
          label="Title"
          placeholder="Choose a title for your story"
          // value={this.props.name}
          // onChangeText={text => this.props.employeeUpdate({ prop: 'name', value: text })}
      />
    </CardSection>
    <CardSection>
      <Input
          label="Content"
          placeholder="Add content"
          // value={this.props.name}
          // onChangeText={text => this.props.employeeUpdate({ prop: 'name', value: text })}
          multiline
          numberOfLines={5}
      />
    </CardSection>
    <CardSection>
    <Button onPress={() => console.log("kur")}>
        Submit
    </Button>
    </CardSection>
  </Card>
);

export { StoryScreen };
