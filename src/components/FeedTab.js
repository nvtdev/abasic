import React from 'react';
import { Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { FeedScreen, StoryScreen } from './screens';

const FeedTab = StackNavigator({
  Feed: {
    screen: FeedScreen,
    path: '/',
    navigationOptions: ({ navigation }) => ({
      headerTitle: 'Feed',
      headerRight: (
        <Button
            onPress={() => navigation.navigate('Story')}
            title="Post"
            style={{ marginRight: 50 }}
        />
      )
    }),
  },
  Story: {
    screen: StoryScreen,
    navigationOptions: {
      title: 'Story',
    },
  }
});

export default FeedTab;
