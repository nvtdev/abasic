import firebase from 'firebase';
import {
  STORY_CREATE
} from './types';

export const storyCreate = ({ title, content }) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/stories`)
      .push({ title, content })
      .then(() => {
        dispatch({ type: STORY_CREATE });
        //Actions.storiesList({ type: 'reset' });
      })
  }
}
