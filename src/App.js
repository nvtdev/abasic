import { TabNavigator } from 'react-navigation';
import FeedTab from './components/FeedTab';
import ProfileTab from './components/ProfileTab';
import LoginForm from './components/LoginForm';

const HomeTabs = TabNavigator(
  {
    Feed: {
      screen: FeedTab,
      path: '/feed'
    },
    Profile: {
      screen: ProfileTab,
      path: '/profile'
    }
  },
  {
    tabBarPosition: 'bottom',
    tabBarOptions: {
      // activeTintColor: '#000000',
      // activeBackgroundColor: '#FFFFFF',
      // inactiveTintColor: '#FFFFFF',
      // inactiveBackgroundColor: '#000000'
    }
  }
);

const App = this.state.isLoggedIn ? HomeTabs : LoginForm;

export default App;
